# Translation of ISO-639 (language names) to Danish
#
# This file is distributed under the same license as the iso-codes package.
#
# Copyright (C)
#   Free Software Foundation, Inc., 2000, 2004, 2005.
#   Kenneth Christiansen <kenneth@gnu.org>, 2000.
#   Keld Simonsen <keld@dkuug.dk>, 2000, 2001.
#   Ole Laursen <olau@hardworking.dk>, 2001.
#   Alastair McKinstry <mckinstry@computer.org>, 2001.
#   Claus Hindsgaul <claus_h@image.dk>, 2004, 2005, 2006.
#
# Many of these iso--639 translations were adopted from the state library department:
# http://www.kat-format.dk/danMARC2/Danmarc2.a8.htm
#
# Kontakt dansk@klid.dk før du ændrer i denne fil.
#
msgid ""
msgstr ""
"Project-Id-Version: iso_639 1.9\n"
"Report-Msgid-Bugs-To: Debian iso-codes team <pkg-isocodes-devel@lists.alioth."
"debian.org>\n"
"POT-Creation-Date: 2008-03-08 12:39+0100\n"
"PO-Revision-Date: 2006-09-30 13:38+0200\n"
"Last-Translator: Claus Hindsgaul <claus.hindsgaul@gmail.com>\n"
"Language-Team: Danish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#. name for aar, aa
msgid "Afar"
msgstr "Afar"

#. name for abk, ab
msgid "Abkhazian"
msgstr "Abkhazian"

#. name for ace
msgid "Achinese"
msgstr "Achinesisk"

#. name for ach
msgid "Acoli"
msgstr "Acoli"

#. name for ada
msgid "Adangme"
msgstr "Adangme"

#. name for ady
msgid "Adyghe; Adygei"
msgstr "Adygé; Adygei"

#. name for afa
msgid "Afro-Asiatic (Other)"
msgstr "Afro-asiatiske sprog (øvrige)"

#. name for afh
msgid "Afrihili"
msgstr "Afrihili"

#. name for afr, af
msgid "Afrikaans"
msgstr "Afrikaans"

#. name for ain
msgid "Ainu"
msgstr "Ainu"

#. name for aka, ak
msgid "Akan"
msgstr "Akan"

#. name for akk
msgid "Akkadian"
msgstr "Akkadisk"

#. name for sqi, sq
msgid "Albanian"
msgstr "Albansk"

#. name for ale
msgid "Aleut"
msgstr ""

#. name for alg
msgid "Algonquian languages"
msgstr "Algonkiske sprog"

#. name for alt
msgid "Southern Altai"
msgstr "Sydaltai"

#. name for amh, am
msgid "Amharic"
msgstr "Amharisk"

#. name for ang
msgid "English, Old (ca. 450-1100)"
msgstr "Engelsk, Old- (ca.450-1100)"

#. name for anp
#, fuzzy
msgid "Angika"
msgstr "Akan"

#. name for apa
msgid "Apache languages"
msgstr "Apachesprog"

#. name for ara, ar
msgid "Arabic"
msgstr "Arabisk"

#. name for arc
msgid "Official Aramaic (700-300 BCE); Imperial Aramaic (700-300 BCE)"
msgstr ""

#. name for arg, an
msgid "Aragonese"
msgstr "Færøsk"

#. name for hye, hy
msgid "Armenian"
msgstr "Armensk"

#. name for arn
msgid "Mapudungun; Mapuche"
msgstr ""

#. name for arp
msgid "Arapaho"
msgstr "Arapaho"

#. name for art
msgid "Artificial (Other)"
msgstr "Kunstsprog (Andre)"

#. name for arw
msgid "Arawak"
msgstr "Arawak"

#. name for asm, as
msgid "Assamese"
msgstr "Assamesisk"

#. name for ast
msgid "Asturian; Bable; Leonese; Asturleonese"
msgstr ""

#. name for ath
msgid "Athapascan languages"
msgstr "Athapascan-sprog"

#. name for aus
msgid "Australian languages"
msgstr "Australske sprog"

#. name for ava, av
msgid "Avaric"
msgstr "Avarisk"

#. name for ave, ae
msgid "Avestan"
msgstr "Avestisk"

#. name for awa
msgid "Awadhi"
msgstr "Awadhi"

#. name for aym, ay
msgid "Aymara"
msgstr "Aymará"

#. name for aze, az
msgid "Azerbaijani"
msgstr "Azerbaijansk"

#. name for bad
#, fuzzy
msgid "Banda languages"
msgstr "Munda (øvrige)"

#. name for bai
msgid "Bamileke languages"
msgstr "Bamileke sprog"

#. name for bak, ba
msgid "Bashkir"
msgstr "Bajkirsk"

#. name for bal
msgid "Baluchi"
msgstr "Baluchi"

#. name for bam, bm
msgid "Bambara"
msgstr "Bambara"

#. name for ban
msgid "Balinese"
msgstr "Balinesisk"

#. name for eus, eu
msgid "Basque"
msgstr "Baskisk"

#. name for bas
msgid "Basa"
msgstr "Basa"

#. name for bat
msgid "Baltic (Other)"
msgstr "Baltisk (Andre)"

#. name for bej
msgid "Beja; Bedawiyet"
msgstr ""

#. name for bel, be
msgid "Belarusian"
msgstr "Hviderussisk"

#. name for bem
msgid "Bemba"
msgstr "Bemba"

#. name for ben, bn
msgid "Bengali"
msgstr "Bengali"

#. name for ber
msgid "Berber (Other)"
msgstr "Berberiske sprog (øvrige)"

#. name for bho
msgid "Bhojpuri"
msgstr "Bhojpuri"

#. name for bih, bh
msgid "Bihari"
msgstr "Bihari"

#. name for bik
msgid "Bikol"
msgstr "Bikol"

#. name for bin
#, fuzzy
msgid "Bini; Edo"
msgstr "Bini"

#. name for bis, bi
msgid "Bislama"
msgstr "Bislama"

#. name for bla
msgid "Siksika"
msgstr "Siksika"

#. name for bnt
msgid "Bantu (Other)"
msgstr "Bantu (øvrige)"

#. name for bos, bs
msgid "Bosnian"
msgstr "Bosnisk"

#. name for bra
msgid "Braj"
msgstr "Braj"

#. name for bre, br
msgid "Breton"
msgstr "Bretonsk"

#. name for btk
#, fuzzy
msgid "Batak languages"
msgstr "Maya sprog"

#. name for bua
msgid "Buriat"
msgstr "Buriat"

#. name for bug
msgid "Buginese"
msgstr "Buginesisk"

#. name for bul, bg
msgid "Bulgarian"
msgstr "Bulgarsk"

#. name for mya, my
msgid "Burmese"
msgstr "Murmesisk"

#. name for byn
msgid "Blin; Bilin"
msgstr "Blin; Bilin"

#. name for cad
msgid "Caddo"
msgstr "Caddo"

#. name for cai
msgid "Central American Indian (Other)"
msgstr "Centralamerikansk indisk (Andre)"

#. name for car
#, fuzzy
msgid "Galibi Carib"
msgstr "Galician"

#. name for cat, ca
msgid "Catalan; Valencian"
msgstr "Catalansk; Valensisk"

#. name for cau
msgid "Caucasian (Other)"
msgstr "Kaukasisk (Andre)"

#. name for ceb
msgid "Cebuano"
msgstr "Cebuano"

#. name for cel
msgid "Celtic (Other)"
msgstr "Keltisk (Andre)"

#. name for cha, ch
msgid "Chamorro"
msgstr "Chamorro"

#. name for chb
msgid "Chibcha"
msgstr "Chibcha"

#. name for che, ce
msgid "Chechen"
msgstr "Tjetjensk"

#. name for chg
msgid "Chagatai"
msgstr "Chagatai"

#. name for zho, zh
msgid "Chinese"
msgstr "Kinesisk"

#. name for chk
#, fuzzy
msgid "Chuukese"
msgstr "Chukese"

#. name for chm
msgid "Mari"
msgstr "Mari (Tjeremissisk)"

#. name for chn
msgid "Chinook jargon"
msgstr "Chinook jargon"

#. name for cho
msgid "Choctaw"
msgstr "Choctaw"

#. name for chp
msgid "Chipewyan; Dene Suline"
msgstr ""

#. name for chr
msgid "Cherokee"
msgstr "Cherokee"

#. name for chu, cu
msgid ""
"Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church "
"Slavonic"
msgstr ""

#. name for chv, cv
msgid "Chuvash"
msgstr "Tjuvaskisk"

#. name for chy
msgid "Cheyenne"
msgstr "Cheyenne"

#. name for cmc
msgid "Chamic languages"
msgstr "Chamiksprog"

#. name for cop
msgid "Coptic"
msgstr "Koptisk"

#. name for cor, kw
msgid "Cornish"
msgstr "Cornisk"

#. name for cos, co
msgid "Corsican"
msgstr "Korsikansk"

#. name for cpe
msgid "Creoles and pidgins, English based (Other)"
msgstr "Kreolsk og Pidgin, Engelsk-baserede (øvrige)"

#. name for cpf
msgid "Creoles and pidgins, French-based (Other)"
msgstr "Kreolsk og Pidgin, Fransk-baserede (øvrige)"

#. name for cpp
msgid "Creoles and pidgins, Portuguese-based (Other)"
msgstr "Kreolsk og Pidgin, Portugisisk-baserede (øvrige)"

#. name for cre, cr
msgid "Cree"
msgstr "Cree"

#. name for crh
msgid "Crimean Tatar; Crimean Turkish"
msgstr "Krim-tatarisk; Krim-tyrkisk"

#. name for crp
msgid "Creoles and pidgins (Other)"
msgstr "Kreolsk og Pidgin (øvrige)"

#. name for csb
msgid "Kashubian"
msgstr "Kashubisk"

#. name for cus
msgid "Cushitic (Other)"
msgstr "Kusjitiske sprog (øvrige)"

#. name for ces, cs
msgid "Czech"
msgstr "Tjekkisk"

#. name for dak
msgid "Dakota"
msgstr "Dakota"

#. name for dan, da
msgid "Danish"
msgstr "Dansk"

#. name for dar
msgid "Dargwa"
msgstr "Dargwa"

#. name for day
#, fuzzy
msgid "Land Dayak languages"
msgstr "Munda (øvrige)"

#. name for del
msgid "Delaware"
msgstr "Delaware"

#. name for den
msgid "Slave (Athapascan)"
msgstr "Slave (Athapascan)"

#. name for dgr
msgid "Dogrib"
msgstr "Dogrib"

#. name for din
msgid "Dinka"
msgstr "Dinka"

#. name for div, dv
msgid "Divehi; Dhivehi; Maldivian"
msgstr ""

#. name for doi
msgid "Dogri"
msgstr "Dogri"

#. name for dra
msgid "Dravidian (Other)"
msgstr "Dravidiske sprog (øvrige)"

#. name for dsb
msgid "Lower Sorbian"
msgstr "Nedresorbisk"

#. name for dua
msgid "Duala"
msgstr "Duala"

#. name for dum
msgid "Dutch, Middle (ca. 1050-1350)"
msgstr "Hollandsk, Middelalderligt (ca. 1050-1350)"

#. name for nld, nl
msgid "Dutch; Flemish"
msgstr "Hollandsk; Flamsk"

#. name for dyu
msgid "Dyula"
msgstr "Dyula"

#. name for dzo, dz
msgid "Dzongkha"
msgstr "Dzongkha"

#. name for efi
msgid "Efik"
msgstr "Efik"

#. name for egy
msgid "Egyptian (Ancient)"
msgstr "Oldægyptisk"

#. name for eka
msgid "Ekajuk"
msgstr "Ekajuk"

#. name for elx
msgid "Elamite"
msgstr "Elamitisk"

#. name for eng, en
msgid "English"
msgstr "Engelsk"

#. name for enm
msgid "English, Middle (1100-1500)"
msgstr "Engelsk, Middelalderligt (1100-1500)"

#. name for epo, eo
msgid "Esperanto"
msgstr "Esperanto"

#. name for est, et
msgid "Estonian"
msgstr "Estisk"

#. name for ewe, ee
msgid "Ewe"
msgstr "Ewe"

#. name for ewo
msgid "Ewondo"
msgstr "Ewondo"

#. name for fan
msgid "Fang"
msgstr "Fang"

#. name for fao, fo
msgid "Faroese"
msgstr "Færøsk"

#. name for fat
msgid "Fanti"
msgstr "Fanti"

#. name for fij, fj
msgid "Fijian"
msgstr "Fijisk"

#. name for fil
msgid "Filipino; Pilipino"
msgstr "Filipino; Pilipino"

#. name for fin, fi
msgid "Finnish"
msgstr "Finsk"

#. name for fiu
msgid "Finno-Ugrian (Other)"
msgstr "Finsk-ugriske sprog (øvrige)"

#. name for fon
msgid "Fon"
msgstr "Fon"

#. name for fra, fr
msgid "French"
msgstr "Fransk"

#. name for frm
msgid "French, Middle (ca. 1400-1600)"
msgstr "Fransk, Middelalderligt (ca.1400-1600)"

#. name for fro
msgid "French, Old (842-ca. 1400)"
msgstr "Fransk, Old- (842-ca.1400)"

#. name for frr
msgid "Northern Frisian"
msgstr "Nordfrisisk"

#. name for frs
msgid "Eastern Frisian"
msgstr "Østfrisisk"

#. name for fry, fy
msgid "Western Frisian"
msgstr "Vestfrisisk"

#. name for ful, ff
msgid "Fulah"
msgstr "Fulah"

#. name for fur
msgid "Friulian"
msgstr "Friulian"

#. name for gaa
msgid "Ga"
msgstr "Ga"

#. name for gay
msgid "Gayo"
msgstr "Gayo"

#. name for gba
msgid "Gbaya"
msgstr "Gbaya"

#. name for gem
msgid "Germanic (Other)"
msgstr "Germanske sprog (øvrige)"

#. name for kat, ka
msgid "Georgian"
msgstr "Georgisk"

#. name for deu, de
msgid "German"
msgstr "Tysk"

#. name for gez
msgid "Geez"
msgstr "Geez"

#. name for gil
msgid "Gilbertese"
msgstr "Gilbertansk"

#. name for gla, gd
msgid "Gaelic; Scottish Gaelic"
msgstr "Gællisk; Skotskgællisk"

#. name for gle, ga
msgid "Irish"
msgstr "Irsk"

#. name for glg, gl
msgid "Galician"
msgstr "Galician"

#. name for glv, gv
msgid "Manx"
msgstr "Manx"

#. name for gmh
msgid "German, Middle High (ca. 1050-1500)"
msgstr "Tysk, Middelalderligt øvre (ca.1050-1500)"

#. name for goh
msgid "German, Old High (ca. 750-1050)"
msgstr "Tysk, old- øvre (ca.750-1050)"

#. name for gon
msgid "Gondi"
msgstr "Gondi"

#. name for gor
msgid "Gorontalo"
msgstr "Gorontalo"

#. name for got
msgid "Gothic"
msgstr "Gotisk"

#. name for grb
msgid "Grebo"
msgstr "Grebo"

#. name for grc
msgid "Greek, Ancient (to 1453)"
msgstr "Oldgræsk (til 1453)"

#. name for ell, el
msgid "Greek, Modern (1453-)"
msgstr "Græsk, moderne (1453-)"

#. name for grn, gn
msgid "Guarani"
msgstr "Guarani"

#. name for gsw
msgid "Swiss German; Alemannic; Alsatian"
msgstr ""

#. name for guj, gu
msgid "Gujarati"
msgstr "Gujarati"

#. name for gwi
#, fuzzy
msgid "Gwich´in"
msgstr "Gwich'in"

#. name for hai
msgid "Haida"
msgstr "Haida"

#. name for hat, ht
msgid "Haitian; Haitian Creole"
msgstr "Haitisk; Haitisk Kreolsk"

#. name for hau, ha
msgid "Hausa"
msgstr "Hausa"

#. name for haw
msgid "Hawaiian"
msgstr "Hawaiansk"

#. name for heb, he
msgid "Hebrew"
msgstr "Hebraisk"

#. name for her, hz
msgid "Herero"
msgstr "Herero"

#. name for hil
msgid "Hiligaynon"
msgstr "Hiligaynon"

#. name for him
msgid "Himachali"
msgstr "Himachali"

#. name for hin, hi
msgid "Hindi"
msgstr "Hindi"

#. name for hit
msgid "Hittite"
msgstr "Hittisk"

#. name for hmn
msgid "Hmong"
msgstr "Hmong"

#. name for hmo, ho
#, fuzzy
msgid "Hiri Motu"
msgstr "Hiri Motu"

#. name for hsb
msgid "Upper Sorbian"
msgstr "Øversorbisk"

#. name for hun, hu
msgid "Hungarian"
msgstr "Ungarsk"

#. name for hup
msgid "Hupa"
msgstr "Hupa"

#. name for iba
msgid "Iban"
msgstr "Iban"

#. name for ibo, ig
msgid "Igbo"
msgstr "Igbo"

#. name for isl, is
msgid "Icelandic"
msgstr "Islandsk"

#. name for ido, io
msgid "Ido"
msgstr "Ido"

#. name for iii, ii
#, fuzzy
msgid "Sichuan Yi; Nuosu"
msgstr "Lituaisk"

#. name for ijo
#, fuzzy
msgid "Ijo languages"
msgstr "Manobo sprog"

#. name for iku, iu
msgid "Inuktitut"
msgstr "Inuktitut"

#. name for ile, ie
#, fuzzy
msgid "Interlingue; Occidental"
msgstr "Interlingue"

#. name for ilo
msgid "Iloko"
msgstr "Iloko"

#. name for ina, ia
msgid "Interlingua (International Auxiliary Language Association)"
msgstr ""

#. name for inc
msgid "Indic (Other)"
msgstr "Indiske sprog (øvrige)"

#. name for ind, id
msgid "Indonesian"
msgstr "Indonesisk"

#. name for ine
msgid "Indo-European (Other)"
msgstr "Indo-europæisk (Andre)"

#. name for inh
msgid "Ingush"
msgstr "Engelsk"

#. name for ipk, ik
msgid "Inupiaq"
msgstr "Inupiaq"

#. name for ira
msgid "Iranian (Other)"
msgstr "Iransk (Andre)"

#. name for iro
msgid "Iroquoian languages"
msgstr "Irokesiske sprog"

#. name for ita, it
msgid "Italian"
msgstr "Italiensk"

#. name for jav, jv
msgid "Javanese"
msgstr "Javanesisk"

#. name for jbo
msgid "Lojban"
msgstr "Lojban"

#. name for jpn, ja
msgid "Japanese"
msgstr "Japansk"

#. name for jpr
msgid "Judeo-Persian"
msgstr "Jødisk-persisk"

#. name for jrb
msgid "Judeo-Arabic"
msgstr "Jødisk-arabisk"

#. name for kaa
msgid "Kara-Kalpak"
msgstr "Karakalpakisk"

#. name for kab
msgid "Kabyle"
msgstr "Kabyle"

#. name for kac
msgid "Kachin; Jingpho"
msgstr ""

#. name for kal, kl
msgid "Kalaallisut; Greenlandic"
msgstr "Kalaallisut; Grønlandsk"

#. name for kam
msgid "Kamba"
msgstr "Kamba"

#. name for kan, kn
msgid "Kannada"
msgstr "Kannaresisk"

#. name for kar
#, fuzzy
msgid "Karen languages"
msgstr "Maya sprog"

#. name for kas, ks
msgid "Kashmiri"
msgstr "Kashmirsk"

#. name for kau, kr
msgid "Kanuri"
msgstr "Kanuri"

#. name for kaw
msgid "Kawi"
msgstr "Kawi"

#. name for kaz, kk
msgid "Kazakh"
msgstr "Kasakhisk"

#. name for kbd
msgid "Kabardian"
msgstr "Kabardi"

#. name for kha
msgid "Khasi"
msgstr "Khasi"

#. name for khi
msgid "Khoisan (Other)"
msgstr "Khoisan (øvrige)"

#. name for khm, km
msgid "Central Khmer"
msgstr ""

#. name for kho
msgid "Khotanese"
msgstr "Khotanesisk"

#. name for kik, ki
msgid "Kikuyu; Gikuyu"
msgstr "Kikuyu; Gikuyu"

#. name for kin, rw
msgid "Kinyarwanda"
msgstr "Kinyarwanda"

#. name for kir, ky
#, fuzzy
msgid "Kirghiz; Kyrgyz"
msgstr "Kirgisisk"

#. name for kmb
msgid "Kimbundu"
msgstr "Kimbundu"

#. name for kok
msgid "Konkani"
msgstr "Konkani"

#. name for kom, kv
msgid "Komi"
msgstr "Komi (Syrjænsk)"

#. name for kon, kg
msgid "Kongo"
msgstr "Kongo"

#. name for kor, ko
msgid "Korean"
msgstr "Koreansk"

#. name for kos
msgid "Kosraean"
msgstr "Kosraean"

#. name for kpe
msgid "Kpelle"
msgstr "Kpelle"

#. name for krc
msgid "Karachay-Balkar"
msgstr "Karatjask (Balkarisk)"

#. name for krl
#, fuzzy
msgid "Karelian"
msgstr "Karen"

#. name for kro
#, fuzzy
msgid "Kru languages"
msgstr "Tupi-sprog"

#. name for kru
msgid "Kurukh"
msgstr "Kurukh"

#. name for kua, kj
#, fuzzy
msgid "Kuanyama; Kwanyama"
msgstr "Kuanyama"

#. name for kum
msgid "Kumyk"
msgstr "Kumyk"

#. name for kur, ku
msgid "Kurdish"
msgstr "Kurdisk"

#. name for kut
msgid "Kutenai"
msgstr "Kutenai"

#. name for lad
msgid "Ladino"
msgstr "Ladino"

#. name for lah
msgid "Lahnda"
msgstr "Lahnda"

#. name for lam
msgid "Lamba"
msgstr "Lamba"

#. name for lao, lo
msgid "Lao"
msgstr "Laotisk"

#. name for lat, la
msgid "Latin"
msgstr "Latin"

#. name for lav, lv
msgid "Latvian"
msgstr "Lettisk"

#. name for lez
msgid "Lezghian"
msgstr "Lezghian"

#. name for lim, li
msgid "Limburgan; Limburger; Limburgish"
msgstr ""

# vist papirstørrelse
#. name for lin, ln
msgid "Lingala"
msgstr "Lingala"

#. name for lit, lt
msgid "Lithuanian"
msgstr "Litauisk"

#. name for lol
msgid "Mongo"
msgstr "Mongo"

#. name for loz
msgid "Lozi"
msgstr "Lozi"

#. name for ltz, lb
#, fuzzy
msgid "Luxembourgish; Letzeburgesch"
msgstr "Luxembourgsk"

#. name for lua
msgid "Luba-Lulua"
msgstr "Luba-Lulua"

#. name for lub, lu
msgid "Luba-Katanga"
msgstr "Luba-Katanga"

#. name for lug, lg
msgid "Ganda"
msgstr "Luganda"

#. name for lui
msgid "Luiseno"
msgstr "Luiseño"

#. name for lun
msgid "Lunda"
msgstr "Lunda"

#. name for luo
msgid "Luo (Kenya and Tanzania)"
msgstr "Luo (Kenya og Tanzania)"

#. name for lus
msgid "Lushai"
msgstr "Lushai"

#. name for mkd, mk
msgid "Macedonian"
msgstr "Makedonsk"

#. name for mad
msgid "Madurese"
msgstr "Madurese"

#. name for mag
msgid "Magahi"
msgstr "Magahi"

#. name for mah, mh
msgid "Marshallese"
msgstr "Marshall"

#. name for mai
msgid "Maithili"
msgstr "Maithili"

#. name for mak
msgid "Makasar"
msgstr "Makasar"

#. name for mal, ml
msgid "Malayalam"
msgstr "Malayalam"

#. name for man
msgid "Mandingo"
msgstr "Mandingo"

#. name for mri, mi
msgid "Maori"
msgstr "Maori"

#. name for map
msgid "Austronesian (Other)"
msgstr "Malajo-polynesiske sprog (øvrige)"

#. name for mar, mr
msgid "Marathi"
msgstr "Marathi"

#. name for mas
msgid "Masai"
msgstr "Masai"

#. name for msa, ms
msgid "Malay"
msgstr "Malajisk"

#. name for mdf
msgid "Moksha"
msgstr "Moksha"

#. name for mdr
msgid "Mandar"
msgstr "Mandar"

#. name for men
msgid "Mende"
msgstr "Mende"

#. name for mga
msgid "Irish, Middle (900-1200)"
msgstr "Irsk, Middelalderligt (900-1200)"

#. name for mic
msgid "Mi'kmaq; Micmac"
msgstr "Mi'kmaq; Micmac"

#. name for min
msgid "Minangkabau"
msgstr "Minangkabau"

#. name for mis
#, fuzzy
msgid "Uncoded languages"
msgstr "Munda (øvrige)"

#. name for mkh
msgid "Mon-Khmer (Other)"
msgstr "Mon-Khmer (øvrige)"

#. name for mlg, mg
msgid "Malagasy"
msgstr "Malagasy"

#. name for mlt, mt
msgid "Maltese"
msgstr "Maltesisk"

#. name for mnc
msgid "Manchu"
msgstr "Man"

#. name for mni
#, fuzzy
msgid "Manipuri"
msgstr "Kanuri"

#. name for mno
msgid "Manobo languages"
msgstr "Manobo sprog"

#. name for moh
msgid "Mohawk"
msgstr "Mohawk"

#. name for mol, mo
msgid "Moldavian"
msgstr "Moldavisk"

#. name for mon, mn
msgid "Mongolian"
msgstr "Mongolsk"

#. name for mos
msgid "Mossi"
msgstr "Mossi"

#. name for mul
msgid "Multiple languages"
msgstr "Flere sprog"

#. name for mun
msgid "Munda languages"
msgstr "Munda (øvrige)"

#. name for mus
msgid "Creek"
msgstr "Muskogee"

#. name for mwl
msgid "Mirandese"
msgstr "Mirandesisk"

#. name for mwr
msgid "Marwari"
msgstr "Marwari"

#. name for myn
msgid "Mayan languages"
msgstr "Maya sprog"

#. name for myv
msgid "Erzya"
msgstr "Erza"

#. name for nah
#, fuzzy
msgid "Nahuatl languages"
msgstr "Nubiske sprog"

#. name for nai
msgid "North American Indian"
msgstr "Nordamerikansk indiansk"

#. name for nap
msgid "Neapolitan"
msgstr "Nyt _alias"

#. name for nau, na
msgid "Nauru"
msgstr "Nauru"

#. name for nav, nv
#, fuzzy
msgid "Navajo; Navaho"
msgstr "Navaho"

#. name for nbl, nr
msgid "Ndebele, South; South Ndebele"
msgstr "Ndebele, Syd; Sydndebele"

#. name for nde, nd
msgid "Ndebele, North; North Ndebele"
msgstr "Ndebele, Nord; Nordndebele"

#. name for ndo, ng
msgid "Ndonga"
msgstr "Ndonga"

#. name for nds
msgid "Low German; Low Saxon; German, Low; Saxon, Low"
msgstr ""

#. name for nep, ne
msgid "Nepali"
msgstr "Nepalesisk"

#. name for new
msgid "Newari; Nepal Bhasa"
msgstr ""

#. name for nia
msgid "Nias"
msgstr "Nias"

#. name for nic
msgid "Niger-Kordofanian (Other)"
msgstr "Niger-Congo sprog (øvrige)"

#. name for niu
msgid "Niuean"
msgstr "Niuean"

#. name for nno, nn
msgid "Norwegian Nynorsk; Nynorsk, Norwegian"
msgstr "Norsk Nynorsk: Nynorsk"

#. name for nob, nb
msgid "Norwegian Bokmål; Bokmål, Norwegian"
msgstr "Bokmål-norsk; Bokmål, Norsk"

#. name for nog
msgid "Nogai"
msgstr "Nogajisk"

#. name for non
msgid "Norse, Old"
msgstr "Islandsk, Old-"

#. name for nor, no
msgid "Norwegian"
msgstr "Norsk"

#. name for nqo
msgid "N'ko"
msgstr ""

#. name for nso
msgid "Northern Sotho, Pedi; Sepedi"
msgstr "Nordsotho; Pedi; Sepedi"

#. name for nub
msgid "Nubian languages"
msgstr "Nubiske sprog"

#. name for nwc
msgid "Classical Newari; Old Newari; Classical Nepal Bhasa"
msgstr "Klassisk Newari; Oldnewari; Klassisk Nepalbhasa"

#. name for nya, ny
#, fuzzy
msgid "Chichewa; Chewa; Nyanja"
msgstr "Nyanja"

#. name for nym
msgid "Nyamwezi"
msgstr "Nyamwezisk"

#. name for nyn
msgid "Nyankole"
msgstr "Nyankole"

#. name for nyo
msgid "Nyoro"
msgstr "Nyoro sprog"

#. name for nzi
msgid "Nzima"
msgstr "Nzima"

#. name for oci, oc
msgid "Occitan (post 1500); Provençal"
msgstr "Occitansk (efter 1500); Provençal"

#. name for oji, oj
msgid "Ojibwa"
msgstr "Ojibwa"

#. name for ori, or
msgid "Oriya"
msgstr "Orija"

#. name for orm, om
msgid "Oromo"
msgstr "Oromo"

#. name for osa
msgid "Osage"
msgstr "Osage"

#. name for oss, os
msgid "Ossetian; Ossetic"
msgstr "Ossetisk"

#. name for ota
msgid "Turkish, Ottoman (1500-1928)"
msgstr "Tyrkisk, Ottomansk (1500-1928)"

#. name for oto
msgid "Otomian languages"
msgstr "Otomi sprog"

#. name for paa
msgid "Papuan (Other)"
msgstr "Papua-australske sprog (øvrige)"

#. name for pag
msgid "Pangasinan"
msgstr "Pangasinan"

#. name for pal
msgid "Pahlavi"
msgstr "Pahlavi"

#. name for pam
msgid "Pampanga; Kapampangan"
msgstr ""

#. name for pan, pa
msgid "Panjabi; Punjabi"
msgstr "Punjabi"

#. name for pap
msgid "Papiamento"
msgstr "Papiamento"

#. name for pau
msgid "Palauan"
msgstr "Palauan"

#. name for peo
msgid "Persian, Old (ca. 600-400 B.C.)"
msgstr "Oldpersisk (ca.600-400 f.k.)"

#. name for fas, fa
msgid "Persian"
msgstr "Persisk"

#. name for phi
msgid "Philippine (Other)"
msgstr "Filipinsk (Andre)"

#. name for phn
msgid "Phoenician"
msgstr "Fønikisk"

#. name for pli, pi
msgid "Pali"
msgstr "Pali"

#. name for pol, pl
msgid "Polish"
msgstr "Polsk"

#. name for pon
msgid "Pohnpeian"
msgstr "Ponape"

#. name for por, pt
msgid "Portuguese"
msgstr "Portugisisk"

#. name for pra
msgid "Prakrit languages"
msgstr "Prakrit sprog"

#. name for pro
msgid "Provençal, Old (to 1500)"
msgstr "Provençalsk Gammelt (indtil 1500)"

#. name for pus, ps
#, fuzzy
msgid "Pushto; Pashto"
msgstr "Pashto"

#. name for qaa-qtz
msgid "Reserved for local use"
msgstr "Reserveret til lokalt brug"

#. name for que, qu
msgid "Quechua"
msgstr "Kechua"

#. name for raj
msgid "Rajasthani"
msgstr "Rajasthani"

#. name for rap
msgid "Rapanui"
msgstr "Rapanui"

#. name for rar
msgid "Rarotongan; Cook Island Maori"
msgstr ""

#. name for roa
msgid "Romance (Other)"
msgstr "Romanske sprog (øvrige)"

#. name for roh, rm
#, fuzzy
msgid "Romansh"
msgstr "Romani"

#. name for rom
msgid "Romany"
msgstr "Romani"

#. name for ron, ro
msgid "Romanian"
msgstr "Rumænsk"

#. name for run, rn
msgid "Rundi"
msgstr "Rundi"

#. name for rup
msgid "Aromanian; Arumanian; Macedo-Romanian"
msgstr ""

#. name for rus, ru
msgid "Russian"
msgstr "Russisk"

#. name for sad
msgid "Sandawe"
msgstr "Sandawe"

#. name for sag, sg
msgid "Sango"
msgstr "Sango"

#. name for sah
msgid "Yakut"
msgstr "Yakut"

#. name for sai
msgid "South American Indian (Other)"
msgstr "Sydamerikansk indisk (Andre)"

#. name for sal
msgid "Salishan languages"
msgstr "Salishan sprog"

#. name for sam
msgid "Samaritan Aramaic"
msgstr "Samaritansk"

#. name for san, sa
msgid "Sanskrit"
msgstr "Sanskrit"

#. name for sas
msgid "Sasak"
msgstr "Sasak"

#. name for sat
msgid "Santali"
msgstr "Santali"

#. name for srp, sr
msgid "Serbian"
msgstr "Serbisk"

#. name for scn
msgid "Sicilian"
msgstr "Siciliansk"

#. name for sco
msgid "Scots"
msgstr "Skotsk"

#. name for hrv, hr
msgid "Croatian"
msgstr "Kroatisk"

#. name for sel
msgid "Selkup"
msgstr "Selkupisk"

#. name for sem
msgid "Semitic (Other)"
msgstr "Semitiske sprog (øvrige)"

#. name for sga
msgid "Irish, Old (to 900)"
msgstr "Irsk, old- (til 900)"

#. name for sgn
msgid "Sign Languages"
msgstr "Tegnsprog"

#. name for shn
msgid "Shan"
msgstr "Shan"

#. name for sid
msgid "Sidamo"
msgstr "Sidamo"

#. name for sin, si
msgid "Sinhala; Sinhalese"
msgstr "Sinala; Sinhalesisk"

#. name for sio
msgid "Siouan languages"
msgstr "Siouan sprog"

#. name for sit
msgid "Sino-Tibetan (Other)"
msgstr "Sino-tibetanske sprog (øvrige)"

#. name for sla
msgid "Slavic (Other)"
msgstr "Slavisk (Andre)"

#. name for slk, sk
msgid "Slovak"
msgstr "Slovakisk"

#. name for slv, sl
msgid "Slovenian"
msgstr "Slovensk"

#. name for sma
msgid "Southern Sami"
msgstr "Nordsamisk"

#. name for sme, se
msgid "Northern Sami"
msgstr "Sydsamisk"

#. name for smi
msgid "Sami languages (Other)"
msgstr "Samiske sprog (øvrige)"

#. name for smj
msgid "Lule Sami"
msgstr "Lule-samisk"

#. name for smn
msgid "Inari Sami"
msgstr "Inari-samisk"

#. name for smo, sm
msgid "Samoan"
msgstr "Samoansk"

#. name for sms
msgid "Skolt Sami"
msgstr "Skolt-samisk"

#. name for sna, sn
msgid "Shona"
msgstr "Shona"

#. name for snd, sd
msgid "Sindhi"
msgstr "Sindhi"

#. name for snk
msgid "Soninke"
msgstr "Soninke"

#. name for sog
msgid "Sogdian"
msgstr "Sogdiansk"

#. name for som, so
msgid "Somali"
msgstr "Somalisk"

#. name for son
#, fuzzy
msgid "Songhai languages"
msgstr "Chamiksprog"

#. name for sot, st
msgid "Sotho, Southern"
msgstr "Sotho, Syd"

#. name for spa, es
msgid "Spanish; Castilian"
msgstr ""

#. name for srd, sc
msgid "Sardinian"
msgstr "Sardinsk"

#. name for srn
#, fuzzy
msgid "Sranan Tongo"
msgstr "Ukrainsk"

#. name for srr
msgid "Serer"
msgstr "Serer"

#. name for ssa
msgid "Nilo-Saharan (Other)"
msgstr "Afrikanske sprog syd for Sahara (øvrige)"

#. name for ssw, ss
msgid "Swati"
msgstr "Swati"

#. name for suk
msgid "Sukuma"
msgstr "Sukuma"

#. name for sun, su
msgid "Sundanese"
msgstr "Sundanesisk"

#. name for sus
msgid "Susu"
msgstr "Susu"

#. name for sux
msgid "Sumerian"
msgstr "Sumerisk"

#. name for swa, sw
msgid "Swahili"
msgstr "Swahili"

#. name for swe, sv
msgid "Swedish"
msgstr "Svensk"

#. name for syc
msgid "Classical Syriac"
msgstr ""

#. name for syr
msgid "Syriac"
msgstr "Syrisk"

#. name for tah, ty
msgid "Tahitian"
msgstr "Tahitiansk"

#. name for tai
msgid "Tai (Other)"
msgstr "Tai (øvrige)"

#. name for tam, ta
msgid "Tamil"
msgstr "Tamilsk"

#. name for tat, tt
msgid "Tatar"
msgstr "Tatarisk"

#. name for tel, te
msgid "Telugu"
msgstr "Telugu"

#. name for tem
msgid "Timne"
msgstr "Temne"

#. name for ter
msgid "Tereno"
msgstr "Tereno"

#. name for tet
msgid "Tetum"
msgstr "Tetum"

#. name for tgk, tg
msgid "Tajik"
msgstr "Tajik"

#. name for tgl, tl
msgid "Tagalog"
msgstr "Tagalog"

#. name for tha, th
msgid "Thai"
msgstr "Thai"

#. name for bod, bo
msgid "Tibetan"
msgstr "Tibetansk"

#. name for tig
msgid "Tigre"
msgstr "Tigré"

#. name for tir, ti
msgid "Tigrinya"
msgstr "Tigrinja"

#. name for tiv
msgid "Tiv"
msgstr "iTiv"

#. name for tkl
msgid "Tokelau"
msgstr "Tokelau"

#. name for tlh
msgid "Klingon; tlhIngan-Hol"
msgstr "Klingon; tlhIngan-Hol"

#. name for tli
msgid "Tlingit"
msgstr "Tlingit"

#. name for tmh
msgid "Tamashek"
msgstr "Tamashek"

#. name for tog
msgid "Tonga (Nyasa)"
msgstr "Tonga (Nyasa)"

#. name for ton, to
msgid "Tonga (Tonga Islands)"
msgstr "Tonga (Tongaøerne)"

#. name for tpi
msgid "Tok Pisin"
msgstr "Tok Pisin"

#. name for tsi
msgid "Tsimshian"
msgstr "Tsimshisk"

#. name for tsn, tn
msgid "Tswana"
msgstr "Tswana"

#. name for tso, ts
msgid "Tsonga"
msgstr "Tonga"

#. name for tuk, tk
msgid "Turkmen"
msgstr "Turkmensk"

#. name for tum
msgid "Tumbuka"
msgstr "Tumbuka"

#. name for tup
msgid "Tupi languages"
msgstr "Tupi-sprog"

#. name for tur, tr
msgid "Turkish"
msgstr "Tyrkisk"

#. name for tut
msgid "Altaic (Other)"
msgstr "Tyrkisk-tatariske sprog (øvrige)"

#. name for tvl
msgid "Tuvalu"
msgstr "Tuvalu"

#. name for twi, tw
msgid "Twi"
msgstr "Twi"

#. name for tyv
msgid "Tuvinian"
msgstr "Tuvinian"

#. name for udm
msgid "Udmurt"
msgstr "Udmurt (Votjakkisk)"

#. name for uga
msgid "Ugaritic"
msgstr "Ugaritic"

#. name for uig, ug
#, fuzzy
msgid "Uighur; Uyghur"
msgstr "Uighur"

#. name for ukr, uk
msgid "Ukrainian"
msgstr "Ukrainsk"

#. name for umb
msgid "Umbundu"
msgstr "Umbundu"

#. name for und
msgid "Undetermined"
msgstr "Ubestemt"

#. name for urd, ur
msgid "Urdu"
msgstr "Urdu"

#. name for uzb, uz
msgid "Uzbek"
msgstr "Uzbesisk"

#. name for vai
msgid "Vai"
msgstr "Vai"

#. name for ven, ve
msgid "Venda"
msgstr "Venda"

#. name for vie, vi
msgid "Vietnamese"
msgstr "Vietnamesisk"

#. name for vol, vo
msgid "Volapük"
msgstr "Volapyk"

#. name for vot
msgid "Votic"
msgstr "Votisk"

#. name for wak
msgid "Wakashan languages"
msgstr "Wakashan sprog"

#. name for wal
msgid "Walamo"
msgstr "Walamo"

#. name for war
msgid "Waray"
msgstr "Waray"

#. name for was
msgid "Washo"
msgstr "Washo"

#. name for cym, cy
msgid "Welsh"
msgstr "Walisisk"

#. name for wen
msgid "Sorbian languages"
msgstr "Vendiske sprog"

#. name for wln, wa
msgid "Walloon"
msgstr "Wallonsk"

#. name for wol, wo
msgid "Wolof"
msgstr "Wolof"

#. name for xal
msgid "Kalmyk; Oirat"
msgstr "Kalmyk; Oirat"

#. name for xho, xh
msgid "Xhosa"
msgstr "Xhosa"

#. name for yao
msgid "Yao"
msgstr "Yao"

#. name for yap
msgid "Yapese"
msgstr "Yap"

#. name for yid, yi
msgid "Yiddish"
msgstr "Jiddish"

#. name for yor, yo
msgid "Yoruba"
msgstr "Yoruba"

#. name for ypk
msgid "Yupik languages"
msgstr "Yupik-sprog"

#. name for zap
msgid "Zapotec"
msgstr "Zapotec"

#. name for zbl
msgid "Blissymbols; Blissymbolics; Bliss"
msgstr ""

#. name for zen
msgid "Zenaga"
msgstr "Zenaga"

#. name for zha, za
#, fuzzy
msgid "Zhuang; Chuang"
msgstr "Zhuang"

#. name for znd
#, fuzzy
msgid "Zande languages"
msgstr "Munda (øvrige)"

#. name for zul, zu
msgid "Zulu"
msgstr "Zulu"

#. name for zun
msgid "Zuni"
msgstr "Zuni"

#. name for zxx
msgid "No linguistic content"
msgstr "Intet sprogligt indhold"

#. name for zza
msgid "Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki"
msgstr ""
